package run

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Config struct {
	context     context.Context
	cwd         string
	lookInCwd   bool
	doNotWait   bool
	stdin       bool
	inputstring string
	inputfile   string

	stdout     bool
	outputfile string
	hideoutput bool

	stderr    bool
	errorfile string
	hideerror bool

	doNotRun  bool
	overwrite bool
	createdir bool
	permdir   os.FileMode
	firstpage int
	lastpage  int

	extension string
}

type fc = func(*Config)
type ffc = func(bool) func(*Config)

type Option = any

func Options(options ...Option) []Option {

	if len(options) == 0 {
		return nil
	}
	return options
}

func NewCfg(opts []Option) Config {
	c := new(Config)
	for _, opt := range opts {
		switch op := opt.(type) {
		case fc:
			op(c)
		case ffc:
			op(true)(c)
		default:
			panic(fmt.Errorf("option %v (%T) is of bad type", op, op))
		}
	}
	return *c
}

func WithContext(ctx context.Context) fc {
	return func(c *Config) {
		c.context = ctx
	}
}

func WithCwd(cwd string) fc {
	return func(c *Config) {
		c.cwd = cwd
	}
}

func WithExtension(ext string) fc {
	return func(c *Config) {
		c.extension = ext
	}
}

func WithLookInCwd(tf bool) fc {
	return func(c *Config) {
		c.lookInCwd = tf
	}
}

func WithDoNotWait(tf bool) fc {
	return func(c *Config) {
		c.doNotWait = tf
	}
}

func WithInputFile(path string) fc {
	return func(c *Config) {
		c.stdin = true
		c.inputfile = path
	}
}

func WithInputString(input string) fc {
	return func(c *Config) {
		c.stdin = true
		c.inputstring = input
	}
}

func WithOutputCatch(tf bool) fc {
	return func(c *Config) {
		c.stdout = tf
		if c.stdout {
			c.hideoutput = false
		}
	}
}

func WithOutputFile(path string) fc {
	return func(c *Config) {
		c.hideoutput = false
		c.outputfile = path
		c.stdout = true
	}
}

func WithErrorCatch(tf bool) fc {
	return func(c *Config) {
		c.stderr = tf
		if c.stderr {
			c.hideerror = false
		}
	}
}

func WithErrorFile(path string) fc {

	return func(c *Config) {
		c.hideerror = false
		c.errorfile = path
		c.stderr = true
	}
}

func WithHideOutput(tf bool) fc {
	return func(c *Config) {
		c.hideoutput = tf
	}
}

func WithHideError(tf bool) fc {
	return func(c *Config) {
		c.hideerror = tf
	}
}

func WithDoNotRun(tf bool) fc {
	return func(c *Config) {
		c.doNotRun = tf
	}
}

func WithOverwrite(tf bool) fc {
	return func(c *Config) {
		c.overwrite = tf
	}
}

func WithCreateDir(perm any) fc {
	return func(c *Config) {
		c.permdir = gperm(perm)
		c.createdir = true
	}
}

func WithFirstPage(page int) fc {
	return func(c *Config) {
		c.firstpage = page
	}
}

func WithLastPage(page int) fc {
	return func(c *Config) {
		c.lastpage = page
	}
}

func gperm(perm any) os.FileMode {
	switch nine := perm.(type) {
	case os.FileMode:
		return nine.Perm()
	case uint64:
		return os.FileMode(nine)
	case string:
		if strings.TrimLeft(nine, "1234567890") == "" {
			base := 10
			if strings.HasPrefix(nine, "0") {
				base = 8
			}
			mode, _ := strconv.ParseUint(nine, base, 32)
			return os.FileMode(mode)
		}
		switch nine {
		case "webdir", "webdavdir", "scriptdir", "daemondir":
			nine = "rwxr-x---"
		case "webfile":
			nine = "rwxr-----"
		case "webdavfile":
			nine = "rw-rw----"
		case "scriptfile", "daemonfile":
			nine = "rwxr-x---"
		case "processdir", "tempdir", "tmpdir", "qtechdir":
			nine = "rwxrwx---"
		case "qtechfile", "processfile", "tmpfile", "tempfile":
			nine = "rw-rw----"
		case "nakeddir":
			nine = "rwxrwxrwx"
		case "nakedfile":
			nine = "rw-rw-rw-"
		}
		var perm os.FileMode = 0
		var plus os.FileMode = 0
		for i, c := range nine {
			perm *= 2
			if c == 's' && i == 2 {
				plus += 04000
			}
			if c == 's' && i == 5 {
				plus += 02000
			}
			if c == 't' && i == 8 {
				plus += 01000
			}

			if c == '-' {
				continue
			}
			perm++
		}
		return perm + plus
	}
	return os.FileMode(0)
}
