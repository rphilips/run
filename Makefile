ROOT := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
PROG := $(notdir ${ROOT})

.PHONY: pkg

all: pkg mod fmt build

pkg:
	gobd pkg --mode=lib --quiet && gobd update ${PROG} --mode=lib --quiet
	
test:
	cd ${ROOT} && go test ./...

vet:
	cd ${ROOT} && go vet ./...

fmt:
	cd ${ROOT} && go list -f '{{.Dir}}' ./... | grep -v /vendor/ | xargs -L1 gofmt -l

mod:
	cd ${ROOT} && go mod tidy && go mod vendor

build:
	gobd build ${PROG} --mode=lib


install:
	cd ${ROOT} && go install -v ./...




