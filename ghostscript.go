package run

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func SplitPDF(source string, target string, options ...Option) (err error) {
	config := NewCfg(options)

	if target != "" {
		fi, err := os.Stat(target)
		if err == nil && fi.IsDir() {
			ext := filepath.Ext(source)
			base := strings.TrimSuffix(filepath.Base(source), ext)
			base = strings.ReplaceAll(base, "%x", "x")
			target = filepath.Join(target, base) + "%x.pdf"
		}
	} else {
		ext := filepath.Ext(source)
		base := strings.TrimSuffix(filepath.Base(source), ext)
		base = strings.ReplaceAll(base, "%x", "x")
		target = filepath.Join(target, base) + "%x.pdf"
	}
	source = filepath.Clean(source)
	target = filepath.Clean(target)

	dir := filepath.Dir(target)

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			return fmt.Errorf("problem with current working directory: %w", err)
		}
	}
	_, err = os.Stat(dir)
	if err != nil && !config.createdir {
		return fmt.Errorf("problem with directory %s of target: %w", dir, err)
	}
	if err != nil {
		perm := config.permdir
		if perm == 0 {
			perm = os.ModePerm
		}
		err = os.MkdirAll(dir, perm)
		if err != nil {
			return fmt.Errorf("problem with directory %s of target: %w", dir, err)
		}
	}

	if !strings.Contains(target, "%x") {
		target = strings.TrimSuffix(target, ".pdf")
		target += "%x.pdf"
	}

	work := []string{
		"-dNOPAUSE",
		"-dQUIET",
		"-dBATCH",
		"-sOutputFile=" + target,
		"-sDEVICE=pdfwrite",
	}
	if config.firstpage != 0 {
		work = append(work, fmt.Sprintf("-dFirstPage=%d", config.firstpage))
	}
	if config.lastpage != 0 {
		work = append(work, fmt.Sprintf("-dLastPage=%d", config.lastpage))
	}
	work = append(work, source)
	options = append(options, WithHideError, WithHideOutput)
	_, _, _, err = Run("ghostscript", work, options...)
	if err != nil {
		return fmt.Errorf("cannot split pages %s to %s: %w", source, target, err)
	}
	return nil
}

func RangePDF(source string, target string, options ...Option) (err error) {
	config := NewCfg(options)

	if target != "" {
		fi, err := os.Stat(target)
		if err == nil && fi.IsDir() {
			ext := filepath.Ext(source)
			base := strings.TrimSuffix(filepath.Base(source), ext)
			base = strings.ReplaceAll(base, "%x", "x")
			target = filepath.Join(target, base) + ".pdf"
		}
	} else {
		ext := filepath.Ext(source)
		base := strings.TrimSuffix(filepath.Base(source), ext)
		base = strings.ReplaceAll(base, "%x", "x")
		target = filepath.Join(target, base) + ".pdf"
	}
	source = filepath.Clean(source)
	target = filepath.Clean(target)

	dir := filepath.Dir(target)

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			return fmt.Errorf("problem with current working directory: %w", err)
		}
	}
	_, err = os.Stat(dir)
	if err != nil && !config.createdir {
		return fmt.Errorf("problem with directory %s of target: %w", dir, err)
	}
	if err != nil {
		perm := config.permdir
		if perm == 0 {
			perm = os.ModePerm
		}
		err = os.MkdirAll(dir, perm)
		if err != nil {
			return fmt.Errorf("problem with directory %s of target: %w", dir, err)
		}
	}

	if strings.Contains(target, "%x") {
		target = strings.ReplaceAll(target, "%x", "x")
	}

	work := []string{
		"-dNOPAUSE",
		"-dQUIET",
		"-dBATCH",
		"-sOutputFile=" + target,
		"-sDEVICE=pdfwrite",
	}
	if config.firstpage != 0 {
		work = append(work, fmt.Sprintf("-dFirstPage=%d", config.firstpage))
	}
	if config.lastpage != 0 {
		work = append(work, fmt.Sprintf("-dLastPage=%d", config.lastpage))
	}
	work = append(work, source)
	options = append(options, WithHideError, WithHideOutput)
	_, _, _, err = Run("ghostscript", work, options...)
	if err != nil {
		return fmt.Errorf("cannot create range from pages %s to %s: %w", source, target, err)
	}
	return nil
}
