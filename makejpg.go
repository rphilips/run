package run

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func MakeJPG(source string, target string, options ...Option) (result string, err error) {
	config := NewCfg(options)

	if target == "" {
		ext := filepath.Ext(source)
		target = strings.TrimSuffix(source, ext) + ".jpg"
	}

	if !config.overwrite {
		_, err := os.Stat(target)
		if err == nil {
			return "", fmt.Errorf("target %s exists already", target)
		}
	}

	tmpfile := ""
	for {
		nr := rand.Intn(math.MaxInt - 1)
		tmpfile = filepath.Join(filepath.Dir(target), strconv.Itoa(nr)+"x"+filepath.Base(target))
		_, err := os.Stat(tmpfile)
		if err == nil {
			continue
		}
		if errors.Is(err, os.ErrNotExist) {
			break
		}
	}

	os.Remove(tmpfile)
	defer os.Remove(tmpfile)

	work := []string{
		source,
		tmpfile,
	}

	options = append(options, WithHideError, WithHideOutput)
	_, _, _, err = Run("convert", work, options...)
	if err != nil {
		return "", fmt.Errorf("cannot convert %s to jpg: %w", source, err)
	}

	err = os.Rename(tmpfile, target)
	if err != nil {
		return "", fmt.Errorf("cannot rename %s to %s: %w", tmpfile, target, err)
	}
	return target, nil
}
