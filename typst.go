package run

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func Typst(source string, target string, mode string, options ...Option) (result string, err error) {
	config := NewCfg(options)
	if mode == "" {
		mode = "pdf"
	}
	if strings.HasSuffix(strings.ToLower(target), ".pdf") {
		mode = "pdf"
	}
	if strings.HasSuffix(strings.ToLower(target), ".svg") {
		mode = "svg"
	}
	if target != "" {
		fi, err := os.Stat(target)
		if err == nil && fi.IsDir() {
			ext := filepath.Ext(source)
			target = filepath.Join(target, strings.TrimSuffix(filepath.Base(source), ext)) + "." + mode
		}
	} else {
		ext := filepath.Ext(source)
		target = strings.TrimSuffix(source, ext) + "." + mode
	}
	source = filepath.Clean(source)
	target = filepath.Clean(target)

	dir := filepath.Dir(target)

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			return "", fmt.Errorf("problem with current working directory: %w", err)
		}
	}
	_, err = os.Stat(dir)
	if err != nil && !config.createdir {
		return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
	}
	if err != nil {
		perm := config.permdir
		if perm == 0 {
			perm = os.ModePerm
		}
		err = os.MkdirAll(dir, perm)
		if err != nil {
			return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
		}
	}

	fi, err := os.Stat(target)
	if err == nil {
		if !fi.Mode().IsRegular() {
			return "", fmt.Errorf("target %s exists but is not a regular file", target)
		}
		_, err := os.OpenFile(target, os.O_RDWR, 0666)
		if err != nil {
			return "", fmt.Errorf("target %s exists but cannot be written: %w", target, err)
		}
	}

	work := []string{
		"compile",
		source,
		target,
	}

	options = append(options, WithHideOutput)
	_, errlog, _, err := Run("typst", work, options...)
	compileerr := errlog.String()
	if err != nil || len(compileerr) > 5 {
		return "", fmt.Errorf("cannot compile1 %s to %s: %w\n\n%s", source, target, err, compileerr)
	}

	return target, nil
}
