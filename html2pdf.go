package run

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func HTML2PDF(source string, target string, options ...Option) (result string, err error) {
	config := NewCfg(options)

	if target != "" {
		fi, err := os.Stat(target)
		if err == nil && fi.IsDir() {
			ext := filepath.Ext(source)
			target = filepath.Join(target, strings.TrimSuffix(filepath.Base(source), ext)) + ".pdf"
		}
	} else {
		ext := filepath.Ext(source)
		target = strings.TrimSuffix(source, ext) + ".pdf"
	}
	source = filepath.Clean(source)
	target = filepath.Clean(target)

	dir := filepath.Dir(target)

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			return "", fmt.Errorf("problem with current working directory: %w", err)
		}
	}
	_, err = os.Stat(dir)
	if err != nil && !config.createdir {
		return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
	}
	if err != nil {
		perm := config.permdir
		if perm == 0 {
			perm = os.ModePerm
		}
		err = os.MkdirAll(dir, perm)
		if err != nil {
			return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
		}
	}

	fi, err := os.Stat(target)
	if err == nil {
		if !fi.Mode().IsRegular() {
			return "", fmt.Errorf("target %s exists but is not a regular file", target)
		}
		_, err := os.OpenFile(target, os.O_RDWR, 0666)
		if err != nil {
			return "", fmt.Errorf("target %s exists but cannot be written: %w", target, err)
		}
	}

	work := []string{
		"--headless",
		"--print-to-pdf=" + target,
		"--print-to-pdf-no-header",
		"--allow-file-access-from-files",
		source,
	}

	options = append(options, WithHideError, WithHideOutput)
	_, _, _, err = Run("chromium", work, options...)
	if err != nil {
		return "", fmt.Errorf("cannot convert %s to %s: %w", source, target, err)
	}
	return target, nil
}
