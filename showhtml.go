package run

import (
	"fmt"
)

func ShowHTML(source string, options ...Option) (err error) {
	work := []string{
		"show",
		source,
	}
	options = append(options, WithHideError, WithHideOutput)
	_, _, _, err = Run("showhtml", work, options...)
	if err != nil {
		return fmt.Errorf("cannot show: %s, error: %s", source, err)
	}
	return nil
}
