//go:build linux || darwin
// +build linux darwin

package run

import (
	"os"
	"strings"
	"testing"
)

func ispath(pth string) bool {
	_, err := os.Stat(pth)
	return err == nil
}
func TestRun01(t *testing.T) {

	out, _, cmd, err := Run("ls", []string{"--help"}, WithOutputCatch)
	if err != nil {
		t.Errorf(`test should succeed`)
	}
	if !strings.Contains(out.String(), "long list") {
		t.Errorf(`test should succeed`)
	}
	if cmd == nil {
		t.Errorf(`test should succeed`)
	}

}

func TestHTML01(t *testing.T) {

	_, err := HTML2PDF("html2pdf/chess.html", "html2pdf")
	if err != nil {
		t.Errorf(`test should succeed`)
	}

}

func TestHTML2Doc01(t *testing.T) {

	r, err := HTML2DOC("html2doc/parochieblad.html", "html2doc/parochieblad", WithExtension(".docx"))
	if err != nil {
		t.Errorf(`test should succeed: %s`, r)
	}

}

func TestSplit01(t *testing.T) {

	err := SplitPDF("ghostscript/R.pdf", "ghostscript", WithFirstPage(2))
	if err != nil {
		t.Errorf(`test should succeed`)
	}

}

func TestRange01(t *testing.T) {

	err := RangePDF("ghostscript/R.pdf", "ghostscript/Range.pdf", WithFirstPage(2))
	if err != nil {
		t.Errorf(`test should succeed`)
	}

}

func TestMakeJPG01(t *testing.T) {

	abspath, err := MakeJPG("makejpg/logo.png", "", WithOverwrite)
	if err != nil {
		t.Errorf(`test should succeed: %s`, err)
	}
	if !ispath(abspath) {
		t.Errorf(`%s should exist`, abspath)
	} else {
		os.Remove(abspath)
	}

}

func TestResizeJPG01(t *testing.T) {

	abspath, err := ResizeJPG("resizejpg/logo.jpg", "resizejpg/smalllogo.jpg", 50, WithOverwrite)
	if err != nil {
		t.Errorf(`test should succeed: %s`, err)
	}
	if !ispath(abspath) {
		t.Errorf(`%s should exist`, abspath)
	} else {
		os.Remove(abspath)
	}
}

func TestFirefox(t *testing.T) {

	err := Firefox("http://w3c.org")
	if err != nil {
		t.Errorf(`test should succeed: %s`, err)
	}

}

func TestTypst(t *testing.T) {
	pdf, err := Typst("typst/good.typ", "", "")
	if err != nil {
		t.Errorf(`test should succeed: %s`, err)
	}
	if !ispath(pdf) {
		t.Errorf(`%s should exist`, pdf)
	} else {
		os.Remove(pdf)
	}
	pdf, err = Typst("typst/bad.typ", "", "", WithErrorFile("typst/error.txt"))
	if err == nil {
		t.Errorf(`test should NOT succeed: %s`, err)
	}
	if ispath(pdf) {
		t.Errorf(`%s should not exist`, pdf)
	} else {
		os.Remove(pdf)
		os.Remove("typst/error.txt")
	}
}

func TestSQLite2DB(t *testing.T) {

	_, err := SQLite2CSV("sqlite2csv/car.db", "Brands", "")
	if err != nil {
		t.Errorf(`test should succeed: %s`, err)
	}

}

func TestShowHTML(t *testing.T) {

	err := ShowHTML("<html>Hello</html>")
	if err != nil {
		t.Errorf(`test should succeed: %s`, err)
	}

}
