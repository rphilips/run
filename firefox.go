package run

func Firefox(url string) (err error) {

	_, _, _, err = Run("firefox", []string{"--new-window", url}, WithDoNotWait)

	return err
}
