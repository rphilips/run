package run

import (
	"fmt"
)

func VSCode(workspace string, fname string, options ...Option) (err error) {

	work := []string{
		workspace,
		"--goto",
		fname + ":1",
	}
	_, _, _, err = Run("code", work, options...)
	if err != nil {
		return fmt.Errorf("cannot open VScode in %s with workspace %s: %w", fname, workspace, err)
	}
	return nil
}
