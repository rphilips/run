package run

import (
	"io"
)

// Run executes an external binary.
// It is a convenience utility which takes care of the details.
// There are 2 required arguments to the function and a number of options:
//
//			program: the name of the binary (if empty, the name of the current program is used).
//				       If the name contains a path separator, it is a path to the program file
//			           If 'lookincwd=yes' is one of the option, the binary is searched in the current working directory
//		    args: slice (or nil) with the arguments. Do not include the name of the program.
//		    options: variadic list of options.
//              - WithContext(context.Context): gives the working context
//              - WithCwd(string): specifies working directory
//              - WithLookInCwd()
//              - WithDoNotWait()
//              - WithStdin(string): path name of input file
//              - WithInput(string): input as a string
//              - WithStdout()
//              - WithStderr()
// The function returns:
//
//	stdout: a *bytes.Buffer if 'WithStdout()'. stdout.Bytes() is then the contents in bytes
//	stderr: a *bytes.Buffer if 'WithStderr()'. stderr.Bytes() is then the contents in bytes
//	cmd: the *exec.Cmd of the proces.
//	err: error message
//
// Examples:
//
// run.Run("ls", []string{"--help"}, run.WithStdout(), run.WithDoNotWait())

func WriteToPipe(program string, args []string, options ...Option) (io.WriteCloser, error) {
	options = append(options, WithDoNotRun)
	_, _, cmd, err := Run(program, args, options...)
	if err != nil {
		return nil, err
	}
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, err
	}
	return stdin, nil
}
