package run

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// Run executes an external binary.
// It is a convenience utility which takes care of the details.
// There are 2 required arguments to the function and a number of options:
//
//			program: the name of the binary (if empty, the name of the current program is used).
//				       If the name contains a path separator, it is a path to the program file
//			           If 'lookincwd=yes' is one of the option, the binary is searched in the current working directory
//		    args: slice (or nil) with the arguments. Do not include the name of the program.
//		    options: variadic list of options.
//              - WithContext(context.Context): gives the working context
//              - WithCwd(path): specifies working directory
//              - WithLookInCwd: look for the binary also in the current working directory
//              - WithDoNotWait: returns without waiting to the exit of the binary
//
//              - WithInputFile(path): path name of input file
//              - WithInputString(string): input as a string
//
//              - WithHideOutput: no output
//              - WithOutputFile(path): captures output in file
//              - WithOutputCatch: returns a *bytes.Buffer, stdout.Bytes() is then the contents in bytes
//
//              - WithHideError no errors
//              - WithErrorFile(path): captures errors in file
//              - WithErrorCatch: returns a *bytes.Buffer, stderr.Bytes() is then the contents in bytes
// The function returns:
//
//	stdout: a *bytes.Buffer
//	stderr: a *bytes.Buffer
//	cmd: the *exec.Cmd of the proces.
//	err: error message
//
// Examples:
//
// run.Run("ls", []string{"--help"}, run.WithOutputFile("/home/rphilips/myoutput.txt"), run.WithDoNotWait)

func Run(program string, args []string, options ...Option) (stdout *bytes.Buffer, stderr *bytes.Buffer, cmd *exec.Cmd, err error) {
	config := NewCfg(options)

	if program == "" {
		program, err = os.Executable()
		if err != nil {
			return nil, nil, nil, err
		}
	}
	program = filepath.FromSlash(program)
	cwd := config.cwd
	if strings.ContainsRune(program, os.PathSeparator) {
		program, err = exec.LookPath(program)
		if err != nil {
			return nil, nil, nil, fmt.Errorf("on searching for %s; got error: %s", program, err)
		}
	} else {
		incwd := config.lookInCwd
		if incwd {
			program, err = exec.LookPath(filepath.Join(cwd, program))
			if err != nil {
				program, err = exec.LookPath(program)
				if err != nil {
					return nil, nil, nil, fmt.Errorf("on searching for %s; got error: %s", program, err)
				}
			}
		} else {
			program, err = exec.LookPath(program)
			if err != nil {
				return nil, nil, nil, fmt.Errorf("on searching for %s; got error: %s", program, err)
			}
		}
	}

	if config.context == nil {
		cmd = exec.Command(program, args...)
	} else {
		cmd = exec.CommandContext(config.context, program, args...)
	}

	// cwd
	cmd.Dir = config.cwd

	// stdin
	switch {
	case config.stdin && config.inputfile == "":
		cmd.Stdin = strings.NewReader(config.inputstring)
	case config.inputfile != "":
		ioqin, err := os.Open(config.inputfile)
		if err != nil {
			return nil, nil, nil, fmt.Errorf("cannot open `%s` for reading", config.inputfile)
		}
		defer ioqin.Close()
		cmd.Stdin = ioqin
	}

	// stdout
	stdout = new(bytes.Buffer)
	switch {
	case config.hideoutput:
		cmd.Stdout = io.Discard
	case config.stdout && config.outputfile == "":
		cmd.Stdout = bufio.NewWriter(stdout)
	case config.outputfile != "" && config.outputfile != "-":
		out, err := os.Create(config.outputfile)
		if err != nil {
			return nil, nil, nil, fmt.Errorf("cannot open `%s` for writing output", config.outputfile)
		}
		cmd.Stdout = out
		defer out.Close()
	default:
		cmd.Stdout = os.Stdout
	}

	// stderr
	stderr = new(bytes.Buffer)
	switch {
	case config.hideerror:
		cmd.Stderr = io.Discard
	case config.stderr && config.errorfile == "":
		cmd.Stderr = bufio.NewWriter(stderr)
	case config.errorfile != "" && config.errorfile != "-":
		eout, err := os.Create(config.errorfile)
		if err != nil {
			return nil, nil, nil, fmt.Errorf("cannot open `%s` for writing errors", config.errorfile)
		}
		cmd.Stderr = eout
		defer eout.Close()
	default:
		cmd.Stderr = os.Stderr
	}

	// do not run

	if config.doNotRun {
		return nil, nil, cmd, nil
	}

	// wait or not

	if config.doNotWait {
		err = cmd.Start()
	} else {
		err = cmd.Run()
		if err != nil {
			err = fmt.Errorf("error on running %v: %s", cmd, err)
		}
	}

	return stdout, stderr, cmd, err
}
