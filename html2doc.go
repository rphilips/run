package run

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func HTML2DOC(source string, target string, options ...Option) (result string, err error) {
	config := NewCfg(options)

	rext := config.extension
	if rext == "" {
		rext = ".odt"
	}
	if !strings.HasPrefix(rext, ".") {
		rext = "." + rext
	}

	ext := filepath.Ext(source)
	if target == "" {
		target = strings.TrimSuffix(source, ext) + rext
	} else {
		fi, err := os.Stat(target)
		if err == nil && fi.IsDir() {
			target = filepath.Join(target, strings.TrimSuffix(filepath.Base(source), ext)) + rext
		}
	}

	text := filepath.Ext(target)
	if text == "" {
		target += rext
	} else {
		rext = text
	}

	source = filepath.Clean(source)
	target = filepath.Clean(target)

	dir := filepath.Dir(target)

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			return "", fmt.Errorf("problem with current working directory: %w", err)
		}
	}

	_, err = os.Stat(dir)
	if err != nil && !config.createdir {
		return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
	}

	if err != nil {
		perm := config.permdir
		if perm == 0 {
			perm = os.ModePerm
		}
		err = os.MkdirAll(dir, perm)
		if err != nil {
			return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
		}
	}

	fi, err := os.Stat(target)
	if err == nil {
		if !fi.Mode().IsRegular() {
			return "", fmt.Errorf("target %s exists but is not a regular file", target)
		}
		_, err := os.OpenFile(target, os.O_RDWR, 0666)
		if err != nil {
			return "", fmt.Errorf("target %s exists but cannot be written: %w", target, err)
		}
	}

	os.Remove(target)

	tmpdir, err := os.MkdirTemp("", "libre*")
	if err != nil {
		return "", fmt.Errorf("cannot create temporary dir")
	}
	defer os.RemoveAll(tmpdir)

	work := []string{
		"--headless",
		"--nologo",
		"--nolockcheck",
		"--norestore",
		"--convert-to",
		"odt",
		"--outdir",
		tmpdir,
		source,
	}

	options = append(options, WithHideError, WithHideOutput)
	_, _, _, err = Run("libreoffice", work, options...)
	if err != nil {
		return "", fmt.Errorf("cannot convert %s to odt: %w", source, err)
	}

	kans, _ := filepath.Glob(filepath.Join(tmpdir, "*.odt"))

	if len(kans) != 1 {
		return "", fmt.Errorf("cannot convert %s to odt", source)
	}
	imfile := kans[0]

	if rext != ".odt" {
		work := []string{
			"--headless",
			"--nologo",
			"--nolockcheck",
			"--norestore",
			"--convert-to",
			filepath.Ext(target)[1:],
			"--outdir",
			tmpdir,
			imfile,
		}
		_, _, _, err = Run("libreoffice", work, options...)
		if err != nil {
			return "", fmt.Errorf("cannot convert %s to %s: %w", source, target, err)
		}
		kans, _ := filepath.Glob(filepath.Join(tmpdir, "*"+rext))

		if len(kans) != 1 {
			return "", fmt.Errorf("cannot convert odt to %s", target)
		}
		imfile = kans[0]
	}

	err = os.Rename(imfile, target)
	if err != nil {
		return "", fmt.Errorf("cannot rename %s to %s: %w", imfile, target, err)
	}
	return target, nil
}
