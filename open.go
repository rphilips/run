package run

import (
	"fmt"
	"path/filepath"
)

func Open(path string, options ...Option) (err error) {

	path, err = filepath.Abs(path)
	if err != nil {
		return err
	}

	work := []string{path}

	_, _, _, err = Run("open", work, options...)
	if err != nil {
		return fmt.Errorf("cannot open %s: %w", path, err)
	}
	return nil
}
