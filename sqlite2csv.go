package run

import (
	"fmt"
	"os"
	"path/filepath"
)

func SQLite2CSV(sqlitedb string, table string, target string, options ...Option) (resultpath string, err error) {
	config := NewCfg(options)

	if sqlitedb == "" {
		return "", fmt.Errorf("SQLite databes is not specified")
	}
	fid, err := os.Stat(sqlitedb)
	if err == nil {
		if !fid.Mode().IsRegular() {
			return "", fmt.Errorf("SQLiteDB %s exists but is not a regular file", sqlitedb)
		}
		_, err := os.OpenFile(sqlitedb, os.O_RDONLY, 0666)
		if err != nil {
			return "", fmt.Errorf("source %s exists but cannot be read: %w", sqlitedb, err)
		}
	}

	if target != "" {
		fi, err := os.Stat(target)
		if err == nil && fi.IsDir() {
			target = filepath.Join(target, filepath.Base(table)+".csv")
		}
	} else {
		target = filepath.Join(filepath.Dir(sqlitedb), filepath.Base(table)+".csv")
	}
	sqlitedb = filepath.Clean(sqlitedb)
	target = filepath.Clean(target)

	dir := filepath.Dir(target)

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			return "", fmt.Errorf("problem with current working directory: %w", err)
		}
	}
	_, err = os.Stat(dir)
	if err != nil && !config.createdir {
		return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
	}
	if err != nil {
		perm := config.permdir
		if perm == 0 {
			perm = os.ModePerm
		}
		err = os.MkdirAll(dir, perm)
		if err != nil {
			return "", fmt.Errorf("problem with directory %s of target: %w", dir, err)
		}
	}

	fi, err := os.Stat(target)
	if err == nil {
		if !fi.Mode().IsRegular() {
			return "", fmt.Errorf("target %s exists but is not a regular file", target)
		}
		_, err := os.OpenFile(target, os.O_RDWR, 0666)
		if err != nil {
			return "", fmt.Errorf("target %s exists but cannot be written: %w", target, err)
		}
	}

	work := []string{
		"--header",
		"--csv",
		sqlitedb,
		fmt.Sprintf(`select * from %s;`, table),
	}

	options = append(options, WithHideError, WithOutputFile(target))
	_, _, _, err = Run("sqlite3", work, options...)
	if err != nil {
		return "", fmt.Errorf("cannot run sqlite3 on %s, table %s: %w", sqlitedb, table, err)
	}
	return target, nil
}
